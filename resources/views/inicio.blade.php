@extends('plantilla')

@section('plantilla')
    @if (session('curso'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('curso') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="container">
        <div class="row p-2">
            <h3 class="col-8">Cursos</h3>
            <a href="{{ route('nc') }}" class="btn btn-success col-4">Agregar nuevo curso</a>
        </div>
    </div>
    <table class="table table-hover">
        <tr class="bg-info">
            <th>Id</th>
            <th>Curso</th>
            <th>Duracion</th>
            <th>Categoria</th>
            <th>Inscritos</th>
            <th>Acciones</th>
        </tr>
        <?php
        $ins = $inscritos;
        ?>
        @foreach ($cursos as $curso)
        <tr>
            <td>{{ $curso->id_curso }}</td>
            <td>{{ $curso->curso }}</td>
            <td>{{ $curso->duracion }} horas</td>
            <td>{{ $curso->categoria }}</td>
            <td>{{ $curso->inscritos }}</td>
            <td>
                <a href="{{ route('ec', $curso->id_curso) }}" class="btn btn-sm btn-warning">Editar</a>
                <form action="{{ route('delete_c', $curso->id_curso) }}" method="POST" class="d-inline-block">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    @if ($curso->inscritos()->count() == 0)
                        <input type="submit" value="Eliminar" class="btn btn-sm btn-danger">
                    @else
                        <button class="btn btn-sm btn-danger disabled">Eliminar</button>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <br>
    <div class="container">
        <div class="row p-2">
            <h3 class="col-8">Inscritos</h3>
            <a href="{{ route('ni') }}" class="btn btn-success col-4">Agregar nuevo alumno</a>
        </div>
    </div>
    @if (session('inscrito'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('inscrito') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <table class="table table-hover">
        <tr class="bg-secondary">
            <th>Id</th>
            <th>Nombres</th>
            <th>Ap. Paterno</th>
            <th>Ap. Materno</th>
            <th>Edad</th>
            <th>Curso inscrito</th>
            <th>Acciones</th>
        </tr>
        @foreach ($inscritos as $inscrito)
        <tr>
            <td>{{ $inscrito->id }}</td>
            <td>{{ $inscrito->nombres }}</td>
            <td>{{ $inscrito->papellido }}</td>
            <td>{{ $inscrito->mapellido }}</td>
            <td>{{ $inscrito->edad }}</td>
            <td><?php $it = App\Curso::find($inscrito->id_curso) ?>{{ $it->curso }}</td>
            <td>
                <a href="{{ route('ei', $inscrito->id) }}" class="btn btn-sm btn-warning d-inline-block">Editar</a>
                <form action="{{ route('delete_i', $inscrito->id) }}" method="POST" class="d-inline">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" value="Eliminar" class="btn btn-sm btn-danger">
                </form>
            </td>
        </tr>
        @endforeach
    </table >
    <br>
    <h1>JOIN</h1>
    <ul>
        @foreach ($join as $j)
            <li>{{ $j->nombres }} {{ $j->papellido }} {{ $j->mapellido }} - {{ $j->curso }}</li>
        @endforeach
    </ul>
    <p class="alert alert-primary">Consultas realizadas = {{ count($queries) }}</p>
    <h3>Queries</h3>
    @foreach ($queries as $q)
        <p class="alert alert-dark">
        @php
            print_r($q)
        @endphp
        </p>
    @endforeach
@endsection
