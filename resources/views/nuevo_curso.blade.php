@extends('plantilla')

@section('plantilla')
    <form action="{{ route('create_c') }}" method="POST" class="form-control">
        <h3>Nuevo curso</h3>
        {{ csrf_field() }}
        <label for="curso" class="form-label">Curso:</label>
        <input type="text" placeholder="Nombre curso" name="curso" id="curso" class="form-control" value="{{ old('curso') }}">
        <label for="duracion" class="form-label">Duracion:</label>
        <input type="number" placeholder="Duracion en horas" min="1" name="duracion" step="any" id="duracion" class="form-control" value="{{ old('duracion') }}" min="1" max="50">
        <label for="categoria" class="form-label">Categoria:</label>
        <select name="categoria" id="categoria" class="form-select">
            <option value="">-- Seleccione categoria --</option>
            <option value="Programacion" {{ old('categoria') == 'Programacion' ? 'selected' : '' }}>Programacion</option>
            <option value="FrontEnd" {{ old('categoria') == 'FrontEnd' ? 'selected' : '' }}>FrontEnd</option>
            <option value="BackEnd" {{ old('categoria') == 'BackEnd' ? 'selected' : '' }}>BackEnd</option>
            <option value="Diseño UX" {{ old('categoria') == 'Diseño UX' ? 'selected' : '' }}>Diseño UX</option>
            <option value="Diseño UI" {{ old('categoria') == 'Diseño UI' ? 'selected' : '' }}>Diseño UI</option>
        </select>
        <input type="submit" value="Crear curso" class="btn btn-success mt-2">
        <a href="{{ route('inicio') }}" class="btn btn-secondary mt-2">Volver</a>
    </form>
    <br>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $error }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endforeach
    {{-- @include('errors') --}}
@endsection