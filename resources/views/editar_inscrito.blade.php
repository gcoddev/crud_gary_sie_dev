@extends('plantilla')

@section('plantilla')
    <form action="{{ route('editar_i') }}" method="POST" class="form-control">
        <h3>Nuevo alumno</h3>
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" value="{{ $ins->id }}" name="id">
        <label for="nombres" 
        class="form-label">Nombres:</label>
        <input type="text" name="nombres" id="nombres" class="form-control" placeholder="Nombre completo" value="{{ $ins->nombres }}" required>
        <label for="papellido" class="form-label">Apellido Paterno:</label>
        <input type="text" name="papellido" id="papellido" class="form-control" placeholder="Apellido paterno" value="{{ $ins->papellido }}" required>
        <label for="mapellido" class="form-label">Apellido Materno:</label>
        <input type="text" name="mapellido" id="mapellido" class="form-control" placeholder="Apellido materno" value="{{ $ins->mapellido }}" required>
        <label for="edad" class="form-label">Edad:</label>
        <input type="number" class="form-control" id="edad" name="edad" placeholder="Edad" min="10" max="100" value="{{ $ins->edad }}" required>
        <label for="icurso" class="form-label">Curso inscrito</label>
        <input type="text" name="icurso" id="icurso" value="<?php $ci = App\Curso::find($ins->id_curso) ?>{{ $ci->curso }}" class="form-control" readonly>
        <input type="submit" value="Actualizar alumno" class="btn btn-warning mt-2">
        <a href="{{ route('inicio') }}" class="btn btn-secondary mt-2">Cancelar</a>
    </form>
    <br>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $error }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endforeach
@endsection