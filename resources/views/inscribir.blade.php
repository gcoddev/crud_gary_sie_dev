@extends('plantilla')

@section('plantilla')
    <form action="{{ route('create_i') }}" method="POST" class="form-control">
        <h3>Nuevo alumno</h3>
        {{ csrf_field() }}
        <label for="nombres" class="form-label">Nombres:</label>
        <input type="text" name="nombres" id="nombres" class="form-control" placeholder="Nombre completo" value="{{ old('nombres') }}">
        <label for="papellido" class="form-label">Apellido Paterno:</label>
        <input type="text" name="papellido" id="papellido" class="form-control" placeholder="Apellido paterno" value="{{ old('papellido') }}">
        <label for="mapellido" class="form-label">Apellido Materno:</label>
        <input type="text" name="mapellido" id="mapellido" class="form-control" placeholder="Apellido materno" value="{{ old('mapellido') }}">
        <label for="edad" class="form-label">Edad:</label>
        <input type="number" class="form-control" id="edad" name="edad" placeholder="Edad" min="10" max="100" value="{{ old('edad') }}">
        <label for="icurso" class="form-label">Curso a inscribir</label>
        <select name="icurso" id="icurso" class="form-select">
            <option value="">-- Seleccione el curso --</option>
            @foreach ($cursos as $curso)
                <option value="{{ $curso->id_curso }}" {{ old('icurso') == $curso->id_curso ? 'selected' : '' }}>{{ $curso->curso }} - {{ $curso->categoria }}</option>
            @endforeach
        </select>
        <input type="submit" value="Inscribir" class="btn btn-success mt-2">
        <a href="{{ route('inicio') }}" class="btn btn-secondary mt-2">Volver</a>
    </form>
    <br>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $error }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endforeach
@endsection