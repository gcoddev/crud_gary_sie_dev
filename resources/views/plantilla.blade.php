<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD Laravel</title>
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/Logo_laravel.png') }}" type="image/x-icon">
</head>
<body>
    <div class="container">
        <div class="list-group list-group-horizontal text-center">
            <a href="{{ route('inicio') }}" class="list-group-item list-group-item-action" aria-current="true">Inicio</a>
            <!-- Button trigger modal -->
            <button type="button" class="list-group-item list-group-item-action" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Acerca de
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Acerca de</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{ asset('img/Logo_laravel.png') }}" alt="Logo Laravel" width="70px">
                                </div>
                                <div class="col-9">
                                    <p>CRUD Laravel 5.5.*</p>
                                    <p>Gary - SIE_DEV</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container my-4">
        <h1 class="text-center mb-3">CRUD Laravel Gary</h1>
        @yield('plantilla')
    </div>
    <script>
        function acerca_de() {
            alert('CRUD Gary Laravel - SIE_DEV')
        }
    </script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
</body>
</html>