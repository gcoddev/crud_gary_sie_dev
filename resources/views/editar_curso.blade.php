@extends('plantilla')

@section('plantilla')
    <form action="{{ route('editar_c', $curso->id_curso) }}" method="POST" class="form-control">
        <h3>Editar curso</h3>
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" value="{{ $curso->id_curso }}" name="id">
        <label for="curso" class="form-label">Curso:</label>
        <input type="text" placeholder="Nombre curso" name="curso" id="curso" class="form-control" value="{{ $curso->curso }}">
        <label for="duracion" class="form-label">Duracion en hrs:</label>
        <input type="number" placeholder="Duracion en horas" min="1" name="duracion" id="duracion" class="form-control" value="{{ $curso->duracion }}" min="1" max="50" step="any">
        <label for="categoria" class="form-label">Categoria:</label>
        <select name="categoria" id="categoria" class="form-select">
            <option value="">-- Seleccione categoria --</option>
            <option value="Programacion" <?php if ($curso->categoria == 'Programacion') {
                echo 'selected';
            } ?>>Programacion</option>
            <option value="FrontEnd" <?php if ($curso->categoria == 'FrontEnd') {
                echo 'selected';
            } ?>>FrontEnd</option>
            <option value="BackEnd" <?php if ($curso->categoria == 'BackEnd') {
                echo 'selected';
            } ?>>BackEnd</option>
            <option value="Diseño UX" <?php if ($curso->categoria == 'Diseño UX') {
                echo 'selected';
            } ?>>Diseño UX</option>
            <option value="Diseño UI" <?php if ($curso->categoria == 'Diseño UI') {
                echo 'selected';
            } ?>>Diseño UI</option>
        </select>
        <input type="submit" value="Actualizar" class="btn btn-warning mt-2">
        <a href="{{ route('inicio') }}" class="btn btn-secondary mt-2">Cancelar</a>
    </form>
    <br>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $error }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endforeach
@endsection