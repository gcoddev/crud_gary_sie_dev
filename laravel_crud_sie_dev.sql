-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2022 a las 15:21:01
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel_crud_sie_dev`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `curso` varchar(50) NOT NULL,
  `duracion` float NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `inscritos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `curso`, `duracion`, `categoria`, `inscritos`) VALUES
(1, 'Python', 3.1, 'Programacion', 1),
(3, 'PHP y MySQL', 3, 'BackEnd', 2),
(4, 'Java SE', 3, 'Programacion', 1),
(5, 'Laravel 8', 5, 'BackEnd', 4),
(9, 'JavaScript', 2, 'Programacion', 0),
(11, 'Adobe Photoshop', 5, 'Diseño UI', 0),
(12, 'Figma', 4, 'Diseño UX', 1),
(13, 'Git & GitHub', 1, 'BackEnd', 1),
(14, 'Node JS', 7, 'FrontEnd', 0),
(15, 'Illustrator', 5, 'Diseño UX', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscritos`
--

CREATE TABLE `inscritos` (
  `id` int(11) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `papellido` varchar(30) NOT NULL,
  `mapellido` varchar(30) NOT NULL,
  `edad` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inscritos`
--

INSERT INTO `inscritos` (`id`, `nombres`, `papellido`, `mapellido`, `edad`, `id_curso`) VALUES
(4, 'Ana', 'Layme', 'Limachi', 20, 4),
(18, 'Estefany', 'Quispe', 'Pillco', 27, 5),
(19, 'Alma', 'Quispe', 'Torrez', 22, 5),
(20, 'Mariluz', 'Limachi', 'Alanoca', 20, 5),
(21, 'Monica', 'Choquehuanca', 'Rengipo', 21, 5),
(24, 'Jacqueline', 'Pillco', 'Apaza', 17, 1),
(25, 'Edwin', 'Patana', 'Pillco', 35, 12),
(26, 'Miriam', 'Alanoca', 'Quispe', 18, 3),
(27, 'Juan Jose', 'Perez', 'Ramirez', 25, 3),
(29, 'Raul', 'Patana', 'Gonzales', 23, 13);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indices de la tabla `inscritos`
--
ALTER TABLE `inscritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cinscrito` (`id_curso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `inscritos`
--
ALTER TABLE `inscritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inscritos`
--
ALTER TABLE `inscritos`
  ADD CONSTRAINT `inscritos_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `cursos` (`id_curso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
