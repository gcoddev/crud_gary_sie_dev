<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscrito extends Model
{
    protected $table = 'inscritos';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function cursos() {
        return $this->belongsTo('App\Curso', 'id_curso');
    }
}
