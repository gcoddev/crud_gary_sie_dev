<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'cursos';
    protected $primaryKey = 'id_curso';
    public $timestamps = false;

    public function inscritos() {
        return $this->hasMany('App\Inscrito', 'id_curso', 'id_curso');
    }
}
