<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Curso;
use App\Inscrito;

class controlador extends Controller
{
    public function inicio() {
        $cursos = Curso::get();
        foreach ($cursos as $curso) {
            $inscritos = $curso::findOrFail($curso->id_curso);
            $contador = $curso->inscritos()->count();
            $inscritos->inscritos = $contador;
            $inscritos->save();
        }
        DB::connection()->enableQueryLog();
        $cursos = Curso::get();
        $join = Inscrito::join('cursos', 'cursos.id_curso', '=', 'inscritos.id_curso')
        ->select('inscritos.nombres', 'inscritos.mapellido', 'inscritos.mapellido', 'cursos.curso')
        ->get();
        // $cursos = Curso::with(['inscritos'])->get();
        $inscritos = Inscrito::get();
        $queries = DB::getQueryLog();
        
        return view('inicio', compact('cursos', 'inscritos', 'join', 'queries'));
        // return view('inicio');
    }
    public function nuevo_curso() {
        return view('nuevo_curso');
    }
    public function create_c(Request $request) {
        $validateData = $request->validate([
            'curso' => 'required|max:255',
            'duracion' => 'required',
            'categoria' => 'required'
        ], [
            'curso.required' => 'El nombre del curso es obligatorio',
            'curso.max' => 'Error mas de 255 caracteres',
            'duracion.required' => 'La duracion es obligatoria',
            'categoria.required' => 'Categoria obligatoria',
            'curso.*' => 'Error curso',
            'duracion.*' => 'Error duracion',
            'categoria.*' => 'Error categoria'
        ]);
        $nuevo_curso = new Curso;
        $nuevo_curso->curso = $request->curso;
        $nuevo_curso->duracion = $request->duracion;
        $nuevo_curso->categoria = $request->categoria;
        $nuevo_curso->inscritos = 0;
        $nuevo_curso->save();

        return redirect()->route('inicio')->with('curso', 'Curso agregado correctamente');
    }
    public function nuevo_inscrito() {
        $cursos = Curso::get();
        return view('inscribir', compact('cursos'));
    }
    public function create_i(Request $request) {
        $validateData = $request->validate([
            'nombres' => 'required|max:255',
            'papellido' => 'required|max:255',
            'mapellido' => 'required|max:255',
            'edad' => 'required|max:2',
            'icurso' => 'required'
        ], [
            'nombres.required' => 'El nombre es obligatorio',
            'nombres.max' => 'Error nombres, mas de 255 caracteres',
            'nombres.*' => 'Error nombres',
            'papellido.required' => 'El apellido paterno es obligatorio',
            'papellido.max' => 'Error apellido paterno, mas de 255 caracteres',
            'papellido.*' => 'Error apellido paterno',
            'mapellido.required' => 'El apellido materno es obligatorio',
            'mapellido.max' => 'Error apellido materno, mas de 255 caracteres',
            'mapellido.*' => 'Error apellido materno',
            'edad.required' => 'La edad es obligatoria',
            'edad.max' => 'Error, edad no valido',
            'edad.*' => 'Error edad',
            'icurso.required' => 'El curso es obligatorio',
            'icurso.*' => 'Error curso'
        ]);
        $nuevo_inscrito = new Inscrito;
        $nuevo_inscrito->nombres = $request->nombres;
        $nuevo_inscrito->papellido = $request->papellido;
        $nuevo_inscrito->mapellido = $request->mapellido;
        $nuevo_inscrito->edad = $request->edad;
        $nuevo_inscrito->id_curso = $request->icurso;
        $nuevo_inscrito->save();
        return redirect()->route('inicio')->with('inscrito', 'Alumno inscrito correctamente');
    }
    public function editar_c($id) {
        $curso = Curso::find($id);
        return view('editar_curso', compact('curso'));
    }
    public function update_c(Request $request) {
        $validateData = $request->validate([
            'curso' => 'required|max:255',
            'duracion' => 'required',
            'categoria' => 'required'
        ], [
            'curso.required' => 'El nombre del curso es obligatorio',
            'curso.max' => 'Error mas de 255 caracteres',
            'duracion.required' => 'La duracion es obligatoria',
            'categoria.required' => 'Categoria obligatoria',
            'curso.*' => 'Error curso',
            'duracion.*' => 'Error duracion',
            'categoria.*' => 'Error categoria'
        ]);
        $update_c = Curso::find($request->id);
        $update_c->curso = $request->curso;
        $update_c->duracion = $request->duracion;
        $update_c->categoria = $request->categoria;
        $update_c->save();
        return redirect()->route('inicio')->with('curso', 'Curso actualizado correctamente');
    }
    public function editar_i($id) {
        $ins = Inscrito::findOrFail($id);
        return view('editar_inscrito', compact('ins'));
    }
    public function update_i(Request $request) {
        $validateData = $request->validate([
            'nombres' => 'required|max:255',
            'papellido' => 'required|max:255',
            'mapellido' => 'required|max:255',
            'edad' => 'required|max:2',
            'icurso' => 'required'
        ], [
            'nombres.required' => 'El nombre es obligatorio',
            'nombres.max' => 'Error nombres, mas de 255 caracteres',
            'nombres.*' => 'Error nombres',
            'papellido.required' => 'El apellido paterno es obligatorio',
            'papellido.max' => 'Error apellido paterno, mas de 255 caracteres',
            'papellido.*' => 'Error apellido paterno',
            'mapellido.required' => 'El apellido materno es obligatorio',
            'mapellido.max' => 'Error apellido materno, mas de 255 caracteres',
            'mapellido.*' => 'Error apellido materno',
            'edad.required' => 'La edad es obligatoria',
            'edad.max' => 'Error, edad no valido',
            'edad.*' => 'Error edad',
            'icurso.required' => 'El curso es obligatorio',
            'icurso.*' => 'Error curso'
        ]);
        $update_i = Inscrito::findOrFail($request->id);
        $update_i->nombres = $request->nombres;
        $update_i->papellido = $request->papellido;
        $update_i->mapellido = $request->mapellido;
        $update_i->edad = $request->edad;
        $update_i->save();

        return redirect()->route('inicio')->with('inscrito', 'Alumno actualizado correctamente');
    }
    public function delete_i($id) {
        $eliminar = Inscrito::findOrFail($id);
        $eliminar->delete();
        return back()->with('inscrito', 'Alumno eliminado correctamente');
    }
    public function delete_c($id) {
        $eliminar = Curso::findOrFail($id);
        $eliminar->delete();
        return back()->with('curso', 'Curso eliminado correctamente');
    }
}
