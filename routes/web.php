<?php

// Route::get('/', 'controlador@welcome');
Route::get('/', 'controlador@inicio')->name('inicio');

Route::get('/nuevo-curso', 'controlador@nuevo_curso')->name('nc');

Route::post('/nuevo-curso', 'controlador@create_c')->name('create_c');

Route::get('/nuevo-inscrito', 'controlador@nuevo_inscrito')->name('ni');

Route::post('/nuevo-inscrito', 'controlador@create_i')->name('create_i');

Route::get('/editar_c/{id}', 'controlador@editar_c')->name('ec');

Route::put('/editar_c', 'controlador@update_c')->name('editar_c');

Route::delete('/eliminar_c/{id}', 'controlador@delete_c')->name('delete_c');

Route::get('/editar_i/{id}', 'controlador@editar_i')->name('ei');

Route::put('/editar_i', 'controlador@update_i')->name('editar_i');

Route::delete('/eliminar_i/{id}', 'controlador@delete_i')->name('delete_i');